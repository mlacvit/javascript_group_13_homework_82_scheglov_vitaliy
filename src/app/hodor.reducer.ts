import { createReducer, on } from '@ngrx/store';
import { codeDoor } from './hodor.actions';


const codeTrue = '0000';

const initState = {
  inCorrect: false,
  codeInter: ''
}
export const hodorReducer = createReducer(
  initState,
  on(codeDoor, (state, {code}) => {
    const newCode = state.codeInter + code;
    if (state.codeInter.length >= 4){
      return state;
    }

    const newState = {...state, codeInter: newCode};
    if (newCode === codeTrue){
      newState.inCorrect = true;
    }
    return newState;
  }),

)
