import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { codeDoor } from '../hodor.actions';

@Component({
  selector: 'app-hodor',
  templateUrl: './hodor.component.html',
  styleUrls: ['./hodor.component.css']
})

export class HodorComponent {
  code!: Observable<any>;
  interCode = [''];
  pinCode = '';
  star = [''];
  stars = '';
  answer!: boolean
  constructor(private store: Store<{code: string}>) {
    this.code = store.select('code');
  }

  onCode(char: string) {
      this.interCode.push(char);
      if (this.star.length <= 4){
        this.star.push('*');
      }
  }

  back(){
    this.interCode.pop();
    this.star.length--;
  }

  enter(){
      this.interCode.reduce((sum: string, current: string)  => {
        return this.pinCode = sum + current;
      });
   this.store.dispatch(codeDoor({code: this.pinCode}));
    this.code.subscribe(inc => {
      this.answer = inc.inCorrect;
    })
    this.star.splice(0);
  }


}
